const requestAPIGitlab = (url: string, options: any) => {
    return rp(options)
        .then(gitlabData => {
            return Promise.resolve(gitlabData);
        })
        .catch(err => {
            return Promise.reject(err);
        });
}